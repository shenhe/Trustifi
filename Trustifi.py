import requests

class Trustifi:
    TRUSTIFI_KEY = None
    TRUSTIFI_SECRET = None
    session = requests.session()

    def read_keys(self):
        key, secret = [x.split('=', 1)[1] for x in open(r"secrets\trustifi-store.env").readlines()]
        return [
            key,
            secret
        ]
    # Headers below are common for almost all the requests
    headers = {
        "x-trustifi-key": TRUSTIFI_KEY,
        "x-trustifi-secret": TRUSTIFI_SECRET,
        "Content-Type": "application/json",
        "Connection": "keep-alive"
    }

    def read_keys(self):
        key, secret = [x.split('=', 1)[1] for x in open(r"secrets\trustifi-store.env").readlines()]
        self.TRUSTIFI_KEY = key
        self.TRUSTIFI_SECRET = secret

    def get_emails(
            self,
            email_id: str = ''
    ):
        req = self.session.get(
            f"https://be.trustifi.com/api/i/v1/email/display/info/{email_id}",
            headers=self.headers
        )
        return [
            req.status_code,
            req.content
       ]

    def get_email_events(
            self,
            email_id: str = ''
    ):
        req = self.session.get(
        f"https://be.trustifi.com/api/i/v1/event/{email_id}",
            headers=self.headers
        )


        return [
            req.status_code,
            req.content
        ]

    def get_postmark(
            self,
            email_id: str = ''
    ):

        req = self.session.get(
            f"https://be.trustifi.com/api/i/v1/email/display/postmark/{email_id}",
            headers=self.headers
        )
        return [
            req.status_code,
            req.content
        ]

    def get_campaign_summary(
            self,
            email_id: str = ''
    ):
        req = self.session.get(
            f"https://be.trustifi.com/api/i/v1/email/display/summary/initmass/{email_id}",
            headers=self.headers
        )
        return [
            req.status_code,
            req.content
        ]

    def get_email_summary_csv(
            self,
            email_id: str = '',
            csv_required: bool = False
    ):
        csv = ['', 'csv/'][csv_required]
        req = self.session.get(
            f"https://be.trustifi.com/api/i/v1/email/display/summary/{csv}{email_id}",
            headers=self.headers
        )
        return [
            req.status_code,
            req.content
        ]

    def get_campaign_full_report(
            self,
            email_id: str = ''
    ):
        req = self.session.get(
            f"https://be.trustifi.com/api/i/v1/email/display/summary/recipients/csv/prepare/{email_id}",
            headers=self.headers
        )
        return [
            req.status_code,
            req.content
        ]

    def get_email_full_report(
            self,
            email_id: str = ''
    ):
        req = self.session.get(
            f"https://be.trustifi.com/api/i/v1/email/display/summary/recipients/csv/download/{email_id}",
            headers=self.headers
        )
        return [
            req.status_code,
            req.content
        ]
